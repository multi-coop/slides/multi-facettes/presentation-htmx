---
title: Café multifacettes
subtitle: HTMX, HTML sous stéroïdes
author: Pierre Camilleri
date: 18 juin 2024
title-slide-attributes:
    data-background-image: "static/logo.svg, static/logo_client.svg"
    data-background-size: "auto 10%, auto 12%"
    data-background-position: "95% 95%, 70% 97%"
---

# Plan 

1. Introduction : architecture REST, hypertext-driven design et single-page 
   applications
2. Avantages et inconvénients d'une architecture REST
3. L'apport d'HTMX
4. Démo


# Introduction 

## Disclaimer

- Je ne suis pas un dev front expérimenté
- Je ne suis pas expert d'htmx
- Ca tombe par hasard en même temps que l'annonce de la v2 !


## L'architecture REST

- REST = representational state transfer

>  It means that a server will respond with the representation of a resource 
(today, it will most often be an HTML, XML or JSON document) and that resource 
**will contain hypermedia links** that can be followed to make the state of 
the system change.

:::{.text-nano}
[Source : wikipedia](https://en.wikipedia.org/wiki/REST)
:::

---

Aujourd'hui, on emploie le mot pour toute API HTTP, souvent à contresens.

:::{.text-nano}
Lecture à ce sujet : [REST APIs must be hypertext-driven](https://roy.gbiv.com/untangled/2008/rest-apis-must-be-hypertext-driven)
:::

## Hypertext-driven architecture

> the **simultaneous presentation of information and controls** such that the 
information becomes the affordance through which the user obtains choices and 
selects actions.

--- 

Concrètement, si ça s'adresse à des utilisateurs humains, une API qui renvoie 
du HTML ! 

## Frameworks JS et Single Page Applications (SPA)

Versus *Single page application* (React, Vue, …) :

1. Une requête initiale reçoit un document HTML dépouillé, sans contenu.
2. Ce document contient quelques éléments de substitution, et des liens vers 
   des fichiers JS : le cœur et l'âme de l'application monopage.
3. Le code HTML est mis à jour dynamiquement dans le navigateur pour créer la 
   page web.

--- 

[Un exemple](https://htmx.org/essays/hateoas/#example)

> [In the **RESTful API**], the web browser **does not know about the 
concept** of an overdrawn account or, indeed, even what an account is.

>  In the **JSON API** example, **out-of-band information is necessary** for 
processing and working with the remote resource. [...] This would typically be 
achieved by consulting documentation for the JSON API.


# Avantages et inconvénients d'une architecture REST

## Couplage ? Découplage ?

- L'architecture introduit un couplage fort entre back-end et front-end : la 
  réponse de l'appel d'API *est* le front-end !
- En échange elle offre un découplage entre le serveur hypermedia et le client 
  hypermedia (en pratique pour du HTML : le navigateur)

--- 

### Plus d'interface à maintenir et documenter

- Plus d'interface entre front et back : on peut radicalement changer l'API 
  sans casser l'application

--- 

### Low tech

![ ](./images/react-server-components.jpeg)

--- 

### Et encore

- Server side rendering (Par définition !)
- Agnostique du langage

---

### Les inconvénients

- Problème de latence du réseau
- Si besoin d'une API spécifiquement pour les données, nécessite d'être 
  développée à côté

# L'apport d'HTMX

> The harsh reality is that HTML **never got to the point** that it could, by 
itself, **offer UX approaching the thick client**.

> Pure server-side rendered HTML offered only a simple & clunky Click → 
Request → Page Render model. For reasons I can’t completely understand, HTML 
never moved beyond this **extremely basic UX model**.

:::{.text-nano}
[Source : taking html seriously](https://intercoolerjs.org/2020/01/14/taking-html-seriously)
:::

## Arguments marketings

htmx gives you access to **AJAX**, **CSS Transitions**, **WebSockets** and 
**Server Sent Events** directly in HTML, using attributes, so you can build 
modern user interfaces with the simplicity and power of hypertext.

htmx is **small** (~14k min.gz’d), **dependency-free**, extendable, IE11 
compatible & has reduced code base sizes by 67% when compared with react

## HTML sous stéroïdes

* Why should only `<a>` & `<form>` be able to make HTTP requests?
* Why should only click & submit events trigger them?
* Why should only GET & POST methods be available?
* Why should you only be able to replace the entire screen?
 
By removing these constraints, htmx completes HTML as a hypertext

# Démo

- [Galerie d'exemples htmx](https://htmx.org/examples/)

Exemples sur [https://hypermedia.gallery/](https://hypermedia.gallery/) :

- [Denmark for expats](https://www.denmarkforexpats.com/)
- [Le zénith Saint-Eustache](https://lezenithsteustache.ca/programmation?category=Musique)

# Les mots de la fin

## Bye bye JS

![HOWL stack : Hypermedia On Whatever you'd like](./images/htmx.png)

## Florimond recommande plutôt Turbo

![ ](./images/turbo.gif)

[Turbo](https://turbo.hotwired.dev/)


## Moi, je recommande la collection de meme d'htmx

![ ](./images/fullstack.jpg)


